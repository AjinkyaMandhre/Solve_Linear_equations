/*
 *
 *  Created on: Oct 1, 2017
 *      Author: aj
 */


#include "equations_matrix.hpp"

#include "utilities.hpp"

void create_matrix_unique_variables(vector<string> file_content, map<string, int> &unique_variables,list<string> &sorted_unique_variables)
{
	//matrix

		int size = unique_variables.size();
		bnu::matrix<double> m(size, size);

		vector<int> constants;
		int constant = 0,colLHS=0, i=0;

		list<string> lhs, lhs_without_space, rhs,brokenRhs;

		for(vector<string>::iterator line = file_content.begin(); line != file_content.end(); line++)
		{

			 lhs = util.split_string(*line, "=.+");
			 lhs_without_space = util.split_string(lhs.front(), "\\s+");
			 rhs = util.split_string(*line, ".+=");
			 rhs.pop_front();
			 brokenRhs = util.split_string(rhs.front(), "\\s+");

			//only LHS
			colLHS = util.find_index_position(sorted_unique_variables,lhs_without_space.front());
			m(i, colLHS) = m.at_element(i, colLHS) + 1;
			//only RHS

			for (list<string>::iterator it = brokenRhs.begin(); it != brokenRhs.end(); it++)
			{
				string current = util.remove_spaces(*it);

				if (current == "+")
				{
					continue;
				}

				if (isdigit((current).c_str()[0]))
				{
					int temp;

					temp = atoi(current.c_str());

					constant = constant + temp;

				}
				else
				{
					//find this in the list
					int col = util.find_index_position(sorted_unique_variables, current);
					if (col >= 0)
						m(i, col) = m.at_element(i, col) - 1;
				}

			}
			i++;
			constants.push_back(constant);
			constant = 0;
		}

		//cout << print_Determinant(m);
		//util.print_vector(constants);
		//cout << print_Determinant(m);
		solve_equations(m,constants,unique_variables);
		print_result(unique_variables);

}

void test_input_create_list_sorted_unique_variables(int line_number, map<string, int> &unique_variables, list<string> &sorted_unique_variables, vector<string> file_content)
{

	if (line_number < unique_variables.size())
	{
		cout<< "Cannot produce a unique solution\nContents of the file are"<< endl;

		for (vector<string>::iterator it = file_content.begin();it != file_content.end(); it++)
		{
			cout << *it << endl;
		}
		exit(-1);
	}

	for (map<string, int>::iterator it = unique_variables.begin();it != unique_variables.end(); it++)
	{
		sorted_unique_variables.push_back(it->first);
	}

	sorted_unique_variables.sort();

}

void print_result( map<string,int> result)
{
	for(map<string,int>::iterator it=result.begin();it!=result.end();it++)
	{
		cout<<it->first<<" = "<<it->second<<endl;
	}
}

void solve_equations(bnu::matrix<double>m,vector<int> constants, map<string,int> &result)
{
	vector<int> detValues;

	bnu::matrix<double> denominator_determinant = m;

	map<string,int>::iterator it=result.begin();
	int denominator = determinant(denominator_determinant);

	//int value_of_determinant = determinant(m);

	for(int column=0; column<m.size2();column++,it++)
	{
		bnu::matrix<double>m_copy = m;
		for(int row=0; row<m.size1();row++)
		{
			//cout << constants[row] << "\n";
		//	cout << row << "," << column << "\n";
			m_copy(row,column)= constants[row];

		}
		int numerator = determinant(m_copy);

		int answer_of_equation = numerator / denominator;

		result[it->first]=answer_of_equation;
		//cout<<"answer"<<answer_of_equation<<endl;
	}


}


int print_Determinant(bnu::matrix<double> m)
{
	cout << "Determinant is\n";
	for (int i = 0; i < m.size1(); i++)
	{
		for (int j = 0; j < m.size2(); j++)
		{
			cout << (int) m(i, j) << "\t";
		}
		cout << endl;
	}
	 return  (int) determinant(m);

}

void get_unique_variables(map<string, int> &unique_variables, string line)
{

	list<string> temp;

	temp = util.split_string(line, "\\s+");

	for (list<string>::iterator it = temp.begin(); it != temp.end(); it++)
	{
		string ch = *it;

		if (isalpha(ch[0]))
		{
			if (unique_variables.find(*it) != unique_variables.end()) //insert unique variables into a map
			{
				unique_variables[*it]++;
			}
			else
			{
				unique_variables.insert(pair<string, int>(*it, 1));
			}
		}

	}

}

/*
 * The following code function is cited from
 * http://programmingexamples.net/wiki/CPP/Boost/Math/uBLAS/determinant
 *
 *Input: Matrix
 *output: Determinant value
 * */

double determinant(bnu::matrix<double>& m)
{
	bnu::permutation_matrix<std::size_t> pm(m.size1());
	double det = 1.0;
	if (bnu::lu_factorize(m, pm))
	{
		det = 0.0;
	} else
	{
		for (int i = 0; i < m.size1(); i++)
			det *= m(i, i); // multiply by elements on diagonal
		det = det * determinant_sign(pm);
	}
	return det;
}

/*
 * The following code function is cited from
 * http://programmingexamples.net/wiki/CPP/Boost/Math/uBLAS/determinant
 *This is a supporting function code which is called internally by the determinant function
 *
 * */
int determinant_sign(const bnu::permutation_matrix<std::size_t>& pm)
{
	int pm_sign = 1;
	std::size_t size = pm.size();
	for (std::size_t i = 0; i < size; ++i)
		if (i != pm(i))
			pm_sign *= -1.0; // swap_rows would swap a pair of rows here, so we change sign
	return pm_sign;
}



