//============================================================================
// Name        : Tesla.cpp
// Author      : ajinkya
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include "stdint.h"
#include <iterator>
#include <list>
#include <boost/regex.hpp>
#include <cctype>
#include <ctype.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include "utilities.hpp"
#include "equations_matrix.hpp"
using namespace std;


void open_stream(const char* file_name, ifstream& input)
{
	input.open(file_name);

		if (input.fail())
		{
			cout << "Failed to open " << file_name << endl;
			exit(-1);
		}

}

int main(int argc, char** argv)
{

	ifstream input;

	string line;

	uint32_t line_number = 0;

	list<string> variables, temp;

	vector<string> file_content;

	map<string, int> unique_variables;

	list<string> sorted_unique_variables;

	open_stream(argv[1],input);

	while (getline(input, line))
	{
		if(!line.empty())
		{
			file_content.push_back(line);
			get_unique_variables(unique_variables, line);
			line_number++;
		}
	}
	if(line_number==0)
	{
		cout << "Empty File nothing to solve"<<endl;
	}

	input.close();

	test_input_create_list_sorted_unique_variables(line_number, unique_variables, sorted_unique_variables,file_content);

	create_matrix_unique_variables(file_content, unique_variables,sorted_unique_variables);


	return 0;
}

