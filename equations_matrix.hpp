/*
 * equations_matrix.hpp
 *
 *  Created on: Oct 1, 2017
 *      Author: aj
 */

#ifndef EQUATIONS_MATRIX_HPP_
#define EQUATIONS_MATRIX_HPP_

#include <iostream>
#include <fstream>
#include <string>
#include "stdint.h"
#include <iterator>
#include <list>
#include <boost/regex.hpp>
#include <cctype>
#include <ctype.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
using namespace std;
using namespace boost;
namespace bnu = boost::numeric::ublas;



void create_matrix_unique_variables(vector<string> file_content, map<string, int> &unique_variables,list<string> &sorted_unique_variables);
void test_input_create_list_sorted_unique_variables(int line_number, map<string, int> &unique_variables, list<string> &sorted_unique_variables, vector<string> file_content);
void print_result( map<string,int> result);
void solve_equations(bnu::matrix<double>m,vector<int> constants, map<string,int> &result);
int print_Determinant(bnu::matrix<double> m);
void get_unique_variables(map<string, int> &unique_variables, string line);
double determinant(bnu::matrix<double>& m);
int determinant_sign(const bnu::permutation_matrix<std::size_t>& pm);

#endif /* EQUATIONS_MATRIX_HPP_ */
