Name: Ajinkya Mandhre
Readme file 


I have created the main file for the project named solve_linear_equations.cpp 

This file requires 2 supporting files that I have included In the folder. utilities.cpp and equatons_matrix.cpp

You can directly execute the code on linux terminal.

I have provided the make file for the source code. However it has a dependancy on the boost file libraries and the path needs to be specified in the make file.

Also if you need to install the boost libraries you need to install them first. I have provided a docx file for that as well "Boost_install.odt". The file has steps that are listed on the http://www.boost.org/doc/libs/1_61_0/more/getting_started/unix-variants.html. (Boost Getting Started on Unix Variants - 1.61.0 - Boost C++ Libraries)

how to make changes to Make File to link your boost libraries.

--->edit the flag LIBS  and  HEADER_DIR.

The default paths are specified for my system as shown below. You need to provide the path of regex.a library in the LIBS flag and path to the boost root directory in the HEADER_DIR as per your system i.e. where you have installed the files.

LIBS = /usr/lib/boost/bin.v2/libs/regex/build/gcc-5.4.0/release/link-static/threading-multi/libboost_regex.a

HEADER_DIR = -I /opt/boost_1_61_0

once you edit the make file just run the make command to create the executable. 


to see the output ./executable file_having_equations.name

./solve_linear_equations equation.txt



there is also directly way to create executable using commandline code:

g++ -Wall -I /opt/boost_1_61_0 solve_linear_equations.cpp utilities.cpp equations_matrix.cpp -o solve_linear_equations /usr/lib/boost/bin.v2/libs/regex/build/gcc-5.4.0/release/link-static/threading-multi/libboost_regex.a

just make sure to replace the paths as per your system location of boost file.



TEST cases

Find the following test in the folder to test the code.

1- equation.txt  -> given by you
2- test_case1.txt  -> empty file
3- test_case2.txt -> repeated similar variables twice in 1 equation. 
4- test_case3.txt -> No unique solution

if NO FILE is provided then it will print failed to open. I have not explicitly kept a check for it, it is checked in the the function where we check if the file has been successfully opened or not.



The folder also contains an executable that can be directly used to execute and test the problem 
