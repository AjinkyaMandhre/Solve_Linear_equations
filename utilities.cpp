/*
 * utilities.cpp
 *
 *  Created on: Oct 1, 2017
 *      Author: aj
 */

#include "utilities.hpp"


void utilities::print_list(list<string> variables)
{

	cout << "************************List Print**************************"<< endl;
	for (list<string>::iterator it = variables.begin(); it != variables.end();	it++)
	{
		cout << *it << endl;
	}
	cout << "**********************************************************"<< endl;

}

void utilities::print_vector(vector<int> variables)
{

	cout << "************************vector Print**************************"<< endl;
	for (vector<int>::iterator it = variables.begin(); it != variables.end();it++)
	{
		cout << *it << endl;
	}
	cout << "**********************************************************"<< endl;

}


list<string> utilities::split_string(string line, string pattern)
{

	list<string> tokenized_equation;
	//   boost::regex rgx("=.+"); //WE GET LHS WITH THIS CODE
	boost::regex rgx(pattern);
	boost::sregex_token_iterator iter(line.begin(), line.end(), rgx, -1);
	boost::sregex_token_iterator end;
	for (; iter != end; ++iter)
	{
		tokenized_equation.push_back(*iter);
	}
	return tokenized_equation;
}

string utilities::remove_spaces(string line)
{

	string temp;
	for (int i = 0; i < line.length(); i++)
	{
		if (line[i] != ' ')
		{
			temp = temp + line[i];
		}
	}

	return temp;
}

int utilities::find_index_position(list<string> sorted_unique_variables, string x)
{

	int count = 0;
	for (list<string>::iterator it = sorted_unique_variables.begin();it != sorted_unique_variables.end(); it++)
	{

		if (*it == x)
		{
			return count;
		}
		count++;
	}

	return -1;
}


