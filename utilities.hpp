/*
 * Utilities.hpp
 *
 *  Created on: Oct 1, 2017
 *      Author: aj
 */

#ifndef UTILITIES_HPP_
#define UTILITIES_HPP_


#include <iostream>
#include <fstream>
#include <string>
#include "stdint.h"
#include <iterator>
#include <list>
#include <boost/regex.hpp>
#include <cctype>
#include <ctype.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
using namespace std;
using namespace boost;
namespace bnu = boost::numeric::ublas;


class utilities{

public:

int find_index_position(list<string> sorted_unique_variables, string x);

string remove_spaces(string line);

list<string> split_string(string line, string pattern);

void print_vector(vector<int> variables);

void print_list(list<string> variables);


};

static utilities util;


#endif /* UTILITIES_HPP_ */
