# Compiler flags
CXX = g++
CXXFLAGS = -Wall 

# External cgreen library and where it's located
LIBS = /usr/lib/boost/bin.v2/libs/regex/build/gcc-5.4.0/release/link-static/threading-multi/libboost_regex.a


# Example directory
EXAMPLE_DIR = example


HEADER_DIR = -I /opt/boost_1_61_0


# Default for "make"
all : solve_linear_equations

clean :
	rm -f solve_linear_equations *.o

equations_matrix.o: equations_matrix.cpp 
	$(CXX) $(HEADER_DIR) $(CXXFLAGS) -c equations_matrix.cpp 

utilities.o: utilities.cpp
	$(CXX) $(HEADER_DIR) $(CXXFLAGS) -c utilities.cpp

solve_linear_equations.o: solve_linear_equations.cpp
	$(CXX) $(HEADER_DIR) $(CXXFLAGS) -c solve_linear_equations.cpp

solve_linear_equations: solve_linear_equations.o utilities.o equations_matrix.o
	$(CXX) $(HEADER_DIR) $(CXXFLAGS) $^ -o $@ $(LIBS)



